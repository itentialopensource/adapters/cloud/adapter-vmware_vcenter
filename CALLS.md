## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for VMware vCenter. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for VMware vCenter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the VMware vCenter. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">postComvmwarevcenterinventorydatastoreactionfind(callback)</td>
    <td style="padding:15px">Returns datastore information for the specified datastores. The key in the {@term result} {@term map} is the datastore identifier and the value in the {@term map} is the datastore information.</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/inventory/datastore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postComvmwarevcenterinventorynetworkactionfind(callback)</td>
    <td style="padding:15px">Returns network information for the specified vCenter Server networks. The key in the {@term result} {@term map} is the network identifier and the value in the {@term map} is the network information.</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/inventory/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postComvmwarevcenterisoimageidlibraryItemactionmount(libraryItem, callback)</td>
    <td style="padding:15px">Mounts an ISO image from a content library on a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/iso/image/id:{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postComvmwarevcenterisoimageidvmactionunmount(vm, callback)</td>
    <td style="padding:15px">Unmounts a previously mounted CD-ROM using an ISO image as a backing.</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/iso/image/id:{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComvmwarevcenterovfexportFlag(callback)</td>
    <td style="padding:15px">Returns information about the supported export flags by the server. <p> The supported flags are: <dl> <dt>PRESERVE_MAC</dt> <dd>Include MAC addresses for network adapters.</dd> <dt>EXTRA_CONFIG</dt> <dd>Include extra configuration in OVF export.</dd> </dl> <p> Future server versions might support additional flags.</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/ovf/export-flag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComvmwarevcenterovfimportFlag(callback)</td>
    <td style="padding:15px">Returns information about the import flags supported by the deployment platform. <p> The supported flags are: <dl> <dt>LAX</dt> <dd>Lax mode parsing of the OVF descriptor.</dd> </dl> <p> Future server versions might support additional flags.</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/ovf/import-flag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postComvmwarevcenterovflibraryItem(clientTokensourcetargetcreateSpec, callback)</td>
    <td style="padding:15px">Creates a library item in content library from a virtual machine or virtual appliance. <p> This {@term operation} creates a library item in content library whose content is an OVF package derived from a source virtual machine or virtual appliance, using the supplied create specification. The OVF package may be stored as in a newly created library item or in an in an existing library item. For an existing library item whose content is updated by this {@term operation}, the original content is overwritten. </p></td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/ovf/library-item?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postComvmwarevcenterovflibraryItemidovfLibraryItemIdactiondeploy(ovfLibraryItemId, clientTokentargetdeploymentSpec, callback)</td>
    <td style="padding:15px">Deploys an OVF package stored in content library to a newly created virtual machine or virtual appliance. <p> This {@term operation} deploys an OVF package which is stored in the library item specified by {@param.name ovfLibraryItemId}. It uses the deployment specification in {@param.name deploymentSpec} to deploy the OVF package to the location specified by {@param.name target}. </p></td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/ovf/library-item/id:{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postComvmwarevcenterovflibraryItemidovfLibraryItemIdactionfilter(ovfLibraryItemId, target, callback)</td>
    <td style="padding:15px">Queries an OVF package stored in content library to retrieve information to use when deploying the package. See {@link #deploy}. <p> This {@term operation} retrieves information from the descriptor of the OVF package stored in the library item specified by {@param.name ovfLibraryItemId}. The information returned by the {@term operation} can be used to populate the deployment specification (see {@link ResourcePoolDeploymentSpec} when deploying the OVF package to the deployment target specified by {@param.name target}. </p></td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/vcenter/ovf/library-item/id:{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentercluster(filterfolders, filterclusters, filternames, filterdatacenters, callback)</td>
    <td style="padding:15px">Returns information about at most 1000 visible (subject to permission checks) clusters in vCenter matching the Cluster.FilterSpec.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterclustercluster(cluster, callback)</td>
    <td style="padding:15px">Retrieves information about the cluster corresponding to cluster.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/cluster/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcenterdatacenter(spec, callback)</td>
    <td style="padding:15px">Create a new datacenter in the vCenter inventory</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/datacenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterdatacenter(filterfolders, filterdatacenters, filternames, callback)</td>
    <td style="padding:15px">Returns information about at most 1000 visible (subject to permission checks) datacenters in vCenter matching the Datacenter.FilterSpec.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/datacenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterdatacenterdatacenter(datacenter, callback)</td>
    <td style="padding:15px">Retrieves information about the datacenter corresponding to datacenter.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/datacenter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcenterdatacenterdatacenter(datacenter, force, callback)</td>
    <td style="padding:15px">Delete an empty datacenter from the vCenter Server</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/datacenter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterdatastore(filterdatastores, filterfolders, filternames, filtertypes, filterdatacenters, callback)</td>
    <td style="padding:15px">Returns information about at most 1000 visible (subject to permission checks) datastores in vCenter matching the Datastore.FilterSpec.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/datastore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterdatastoredatastore(datastore, callback)</td>
    <td style="padding:15px">Retrieves information about the datastore indicated by datastore.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/datastore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterfolder(filterfolders, filterparentFolders, filtertype, filternames, filterdatacenters, callback)</td>
    <td style="padding:15px">Returns information about at most 1000 visible (subject to permission checks) folders in vCenter matching the Folder.FilterSpec.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/folder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcenterhost(spec, callback)</td>
    <td style="padding:15px">Add a new standalone host in the vCenter inventory. The newly connected host will be in connected state. The vCenter Server will verify the SSL certificate before adding the host to its inventory. In the case where the SSL certificate cannot be verified because the Certificate Authority is not recognized or the certificate is self signed, the vCenter Server will fall back to thumbprint verification mode as defined by Host.CreateSpec.ThumbprintVerification.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterhost(filterfolders, filterstandalone, filterhosts, filternames, filterclusters, filterdatacenters, filterconnectionStates, callback)</td>
    <td style="padding:15px">Returns information about at most 1000 visible (subject to permission checks) hosts in vCenter matching the Host.FilterSpec.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcenterhosthost(host, callback)</td>
    <td style="padding:15px">Remove a standalone host from the vCenter Server.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/host/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcenterhosthostconnect(host, callback)</td>
    <td style="padding:15px">Connect to the host corresponding to host previously added to the vCenter server.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/host/{pathv1}/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcenterhosthostdisconnect(host, callback)</td>
    <td style="padding:15px">Disconnect the host corresponding to host from the vCenter server</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/host/{pathv1}/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenternetwork(filterfolders, filterdatacenters, filternetworks, filtertypes, filternames, callback)</td>
    <td style="padding:15px">Returns information about at most 1000 visible (subject to permission checks) networks in vCenter matching the Network.FilterSpec.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterresourcePool(filterparentResourcePools, filterhosts, filternames, filterclusters, filterdatacenters, filterresourcePools, callback)</td>
    <td style="padding:15px">Returns information about at most 1000 visible (subject to permission checks) resource pools in vCenter matching the ResourcePool.FilterSpec.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/resource-pool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcenterresourcePoolresourcePool(resourcepool, callback)</td>
    <td style="padding:15px">Retrieves information about the resource pool indicated by resourcePool.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/resource-pool/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervm(spec, callback)</td>
    <td style="padding:15px">Creates a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervm(filterfolders, filterpowerStates, filterhosts, filternames, filterclusters, filterdatacenters, filterresourcePools, filtervms, callback)</td>
    <td style="padding:15px">Returns information about at most 1000 visible (subject to permission checks) virtual machines in vCenter matching the VM.FilterSpec.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvm(vm, callback)</td>
    <td style="padding:15px">Deletes a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvm(vm, callback)</td>
    <td style="padding:15px">Returns information about a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardware(vm, callback)</td>
    <td style="padding:15px">Returns the virtual hardware settings of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardware(vm, spec, callback)</td>
    <td style="padding:15px">Updates the virtual hardware settings of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareactionupgrade(vm, version, callback)</td>
    <td style="padding:15px">Upgrades the virtual machine to a newer virtual hardware version.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/action/upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareadaptersata(vm, spec, callback)</td>
    <td style="padding:15px">Adds a virtual SATA adapter to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/sata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareadaptersata(vm, callback)</td>
    <td style="padding:15px">Returns commonly used information about the virtual SATA adapters belonging to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/sata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvmhardwareadaptersataadapter(vm, adapter, callback)</td>
    <td style="padding:15px">Removes a virtual SATA adapter from the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/sata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareadaptersataadapter(vm, adapter, callback)</td>
    <td style="padding:15px">Returns information about a virtual SATA adapter.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/sata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareadapterscsi(vm, spec, callback)</td>
    <td style="padding:15px">Adds a virtual SCSI adapter to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/scsi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareadapterscsi(vm, callback)</td>
    <td style="padding:15px">Returns commonly used information about the virtual SCSI adapters belonging to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/scsi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareadapterscsiadapter(vm, adapter, callback)</td>
    <td style="padding:15px">Returns information about a virtual SCSI adapter.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/scsi/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwareadapterscsiadapter(vm, adapter, spec, callback)</td>
    <td style="padding:15px">Updates the configuration of a virtual SCSI adapter.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/scsi/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvmhardwareadapterscsiadapter(vm, adapter, callback)</td>
    <td style="padding:15px">Removes a virtual SCSI adapter from the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/adapter/scsi/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareboot(vm, callback)</td>
    <td style="padding:15px">Returns the boot-related settings of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/boot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwareboot(vm, spec, callback)</td>
    <td style="padding:15px">Updates the boot-related settings of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/boot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVcentervmvmhardwarebootdevice(vm, devices, callback)</td>
    <td style="padding:15px">Sets the virtual devices that will be used to boot the virtual machine. The virtual machine will check the devices in order, attempting to boot from each, until the virtual machine boots successfully. If the list is empty, the virtual machine will use a default boot sequence. There should be no more than one instance of Device.Entry for a given device type except ETHERNET in the list.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/boot/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwarebootdevice(vm, callback)</td>
    <td style="padding:15px">Returns an ordered list of boot devices for the virtual machine. If the list is empty, the virtual machine uses a default boot sequence.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/boot/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwarecdrom(vm, spec, callback)</td>
    <td style="padding:15px">Adds a virtual CD-ROM device to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cdrom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwarecdrom(vm, callback)</td>
    <td style="padding:15px">Returns commonly used information about the virtual CD-ROM devices belonging to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cdrom?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwarecdromcdrom(vm, cdrom, spec, callback)</td>
    <td style="padding:15px">Updates the configuration of a virtual CD-ROM device.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cdrom/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwarecdromcdrom(vm, cdrom, callback)</td>
    <td style="padding:15px">Returns information about a virtual CD-ROM device.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cdrom/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvmhardwarecdromcdrom(vm, cdrom, callback)</td>
    <td style="padding:15px">Removes a virtual CD-ROM device from the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cdrom/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwarecdromcdromconnect(vm, cdrom, callback)</td>
    <td style="padding:15px">Connects a virtual CD-ROM device of a powered-on virtual machine to its backing. Connecting the virtual device makes the backing accessible from the perspective of the guest operating system. 
 For a powered-off virtual machine, the Cdrom.update operation may be used to configure the virtual CD-ROM device to start in the connected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cdrom/{pathv2}/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwarecdromcdromdisconnect(vm, cdrom, callback)</td>
    <td style="padding:15px">Disconnects a virtual CD-ROM device of a powered-on virtual machine from its backing. The virtual device is still present and its backing configuration is unchanged, but from the perspective of the guest operating system, the CD-ROM device is not connected to its backing resource. 
 For a powered-off virtual machine, the Cdrom.update operation may be used to configure the virtual CD-ROM device to start in the disconnected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cdrom/{pathv2}/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwarecpu(vm, callback)</td>
    <td style="padding:15px">Returns the CPU-related settings of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cpu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwarecpu(vm, spec, callback)</td>
    <td style="padding:15px">Updates the CPU-related settings of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/cpu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwaredisk(vm, spec, callback)</td>
    <td style="padding:15px">Adds a virtual disk to the virtual machine. While adding the virtual disk, a new VMDK file may be created or an existing VMDK file may be used to back the virtual disk.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/disk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwaredisk(vm, callback)</td>
    <td style="padding:15px">Returns commonly used information about the virtual disks belonging to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/disk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwarediskdisk(vm, disk, callback)</td>
    <td style="padding:15px">Returns information about a virtual disk.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/disk/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvmhardwarediskdisk(vm, disk, callback)</td>
    <td style="padding:15px">Removes a virtual disk from the virtual machine. This operation does not destroy the VMDK file that backs the virtual disk. It only detaches the VMDK file from the virtual machine. Once detached, the VMDK file will not be destroyed when the virtual machine to which it was associated is deleted.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/disk/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwarediskdisk(vm, disk, spec, callback)</td>
    <td style="padding:15px">Updates the configuration of a virtual disk. An update operation can be used to detach the existing VMDK file and attach another VMDK file to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/disk/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareethernet(vm, spec, callback)</td>
    <td style="padding:15px">Adds a virtual Ethernet adapter to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/ethernet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareethernet(vm, callback)</td>
    <td style="padding:15px">Returns commonly used information about the virtual Ethernet adapters belonging to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/ethernet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwareethernetnic(vm, nic, spec, callback)</td>
    <td style="padding:15px">Updates the configuration of a virtual Ethernet adapter.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/ethernet/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareethernetnic(vm, nic, callback)</td>
    <td style="padding:15px">Returns information about a virtual Ethernet adapter.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/ethernet/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvmhardwareethernetnic(vm, nic, callback)</td>
    <td style="padding:15px">Removes a virtual Ethernet adapter from the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/ethernet/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareethernetnicconnect(vm, nic, callback)</td>
    <td style="padding:15px">Connects a virtual Ethernet adapter of a powered-on virtual machine to its backing. Connecting the virtual device makes the backing accessible from the perspective of the guest operating system. 
 For a powered-off virtual machine, the Ethernet.update operation may be used to configure the virtual Ethernet adapter to start in the connected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/ethernet/{pathv2}/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareethernetnicdisconnect(vm, nic, callback)</td>
    <td style="padding:15px">Disconnects a virtual Ethernet adapter of a powered-on virtual machine from its backing. The virtual device is still present and its backing configuration is unchanged, but from the perspective of the guest operating system, the Ethernet adapter is not connected to its backing resource. 
 For a powered-off virtual machine, the Ethernet.update operation may be used to configure the virtual Ethernet adapter to start in the disconnected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/ethernet/{pathv2}/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwarefloppy(vm, spec, callback)</td>
    <td style="padding:15px">Adds a virtual floppy drive to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/floppy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwarefloppy(vm, callback)</td>
    <td style="padding:15px">Returns commonly used information about the virtual floppy drives belonging to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/floppy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvmhardwarefloppyfloppy(vm, floppy, callback)</td>
    <td style="padding:15px">Removes a virtual floppy drive from the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/floppy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwarefloppyfloppy(vm, floppy, spec, callback)</td>
    <td style="padding:15px">Updates the configuration of a virtual floppy drive.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/floppy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwarefloppyfloppy(vm, floppy, callback)</td>
    <td style="padding:15px">Returns information about a virtual floppy drive.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/floppy/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwarefloppyfloppyconnect(vm, floppy, callback)</td>
    <td style="padding:15px">Connects a virtual floppy drive of a powered-on virtual machine to its backing. Connecting the virtual device makes the backing accessible from the perspective of the guest operating system. 
 For a powered-off virtual machine, the Floppy.update operation may be used to configure the virtual floppy drive to start in the connected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/floppy/{pathv2}/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwarefloppyfloppydisconnect(vm, floppy, callback)</td>
    <td style="padding:15px">Disconnects a virtual floppy drive of a powered-on virtual machine from its backing. The virtual device is still present and its backing configuration is unchanged, but from the perspective of the guest operating system, the floppy drive is not connected to its backing resource. 
 For a powered-off virtual machine, the Floppy.update operation may be used to configure the virtual floppy floppy to start in the disconnected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/floppy/{pathv2}/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwarememory(vm, spec, callback)</td>
    <td style="padding:15px">Updates the memory-related settings of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/memory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwarememory(vm, callback)</td>
    <td style="padding:15px">Returns the memory-related settings of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/memory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareparallel(vm, spec, callback)</td>
    <td style="padding:15px">Adds a virtual parallel port to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/parallel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareparallel(vm, callback)</td>
    <td style="padding:15px">Returns commonly used information about the virtual parallel ports belonging to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/parallel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvmhardwareparallelport(vm, port, callback)</td>
    <td style="padding:15px">Removes a virtual parallel port from the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/parallel/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwareparallelport(vm, port, spec, callback)</td>
    <td style="padding:15px">Updates the configuration of a virtual parallel port.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/parallel/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareparallelport(vm, port, callback)</td>
    <td style="padding:15px">Returns information about a virtual parallel port.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/parallel/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareparallelportconnect(vm, port, callback)</td>
    <td style="padding:15px">Connects a virtual parallel port of a powered-on virtual machine to its backing. Connecting the virtual device makes the backing accessible from the perspective of the guest operating system. 
 For a powered-off virtual machine, the Parallel.update operation may be used to configure the virtual parallel port to start in the connected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/parallel/{pathv2}/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareparallelportdisconnect(vm, port, callback)</td>
    <td style="padding:15px">Disconnects a virtual parallel port of a powered-on virtual machine from its backing. The virtual device is still present and its backing configuration is unchanged, but from the perspective of the guest operating system, the parallel port is not connected to its backing. 
 For a powered-off virtual machine, the Parallel.update operation may be used to configure the virtual parallel port to start in the disconnected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/parallel/{pathv2}/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareserial(vm, spec, callback)</td>
    <td style="padding:15px">Adds a virtual serial port to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/serial?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareserial(vm, callback)</td>
    <td style="padding:15px">Returns commonly used information about the virtual serial ports belonging to the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/serial?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVcentervmvmhardwareserialport(vm, port, spec, callback)</td>
    <td style="padding:15px">Updates the configuration of a virtual serial port.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/serial/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmhardwareserialport(vm, port, callback)</td>
    <td style="padding:15px">Returns information about a virtual serial port.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/serial/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVcentervmvmhardwareserialport(vm, port, callback)</td>
    <td style="padding:15px">Removes a virtual serial port from the virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/serial/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareserialportconnect(vm, port, callback)</td>
    <td style="padding:15px">Connects a virtual serial port of a powered-on virtual machine to its backing. Connecting the virtual device makes the backing accessible from the perspective of the guest operating system. 
 For a powered-off virtual machine, the Serial.update operation may be used to configure the virtual serial port to start in the connected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/serial/{pathv2}/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmhardwareserialportdisconnect(vm, port, callback)</td>
    <td style="padding:15px">Disconnects a virtual serial port of a powered-on virtual machine from its backing. The virtual device is still present and its backing configuration is unchanged, but from the perspective of the guest operating system, the serial port is not connected to its backing. 
 For a powered-off virtual machine, the Serial.update operation may be used to configure the virtual serial port to start in the disconnected state when the virtual machine is powered on.
</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/hardware/serial/{pathv2}/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVcentervmvmpower(vm, callback)</td>
    <td style="padding:15px">Returns the power state information of a virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/power?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmpowerreset(vm, callback)</td>
    <td style="padding:15px">Resets a powered-on virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/power/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmpowerstart(vm, callback)</td>
    <td style="padding:15px">Powers on a powered-off or suspended virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/power/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmpowerstop(vm, callback)</td>
    <td style="padding:15px">Powers off a powered-on or suspended virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/power/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmvmpowersuspend(vm, callback)</td>
    <td style="padding:15px">Suspends a powered-on virtual machine.</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm/{pathv1}/power/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVcentervmtemplatedeploy(item, requestBody, callback)</td>
    <td style="padding:15px">Creates a Virtual Machine from the template</td>
    <td style="padding:15px">{base_path}/{version}/vcenter/vm-template/library-items/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findLibrary(action, body, callback)</td>
    <td style="padding:15px">Find Library</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/content/library?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findLibraryItem(action, body, callback)</td>
    <td style="padding:15px">Find Library Item</td>
    <td style="padding:15px">{base_path}/{version}/com/vmware/content/library/item?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
