
## 0.13.5 [10-15-2024]

* Changes made at 2024.10.14_21:35PM

See merge request itentialopensource/adapters/adapter-vmware_vcenter!31

---

## 0.13.4 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-vmware_vcenter!29

---

## 0.13.3 [08-15-2024]

* Changes made at 2024.08.14_19:56PM

See merge request itentialopensource/adapters/adapter-vmware_vcenter!28

---

## 0.13.2 [08-14-2024]

* Add library calls

See merge request itentialopensource/adapters/adapter-vmware_vcenter!27

---

## 0.13.1 [08-07-2024]

* Changes made at 2024.08.06_22:04PM

See merge request itentialopensource/adapters/adapter-vmware_vcenter!26

---

## 0.13.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!25

---

## 0.12.5 [03-28-2024]

* Changes made at 2024.03.28_13:28PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!24

---

## 0.12.4 [03-21-2024]

* Changes made at 2024.03.21_14:05PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!23

---

## 0.12.3 [03-11-2024]

* Changes made at 2024.03.11_15:39PM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!22

---

## 0.12.2 [02-28-2024]

* Changes made at 2024.02.28_11:55AM

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!21

---

## 0.12.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!20

---

## 0.12.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!19

---

## 0.11.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!19

---

## 0.10.0 [10-19-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!18

---

## 0.9.0 [10-02-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!18

---

## 0.8.2 [12-20-2022]

* remove-required-fields-in-schemaTokenReq

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!17

---

## 0.8.1 [12-14-2022]

* remove-required-fields-in-schemaTokenReq

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!17

---

## 0.8.0 [05-16-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!16

---

## 0.7.0 [01-24-2022]

- Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
- Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
- Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
- Add scripts for easier authentication, removing hooks, etc
- Script changes (install script as well as database changes in other scripts)
- Double # of path vars on generic call
- Update versions of foundation (e.g. adapter-utils)
- Update npm publish so it supports https
- Update dependencies
- Add preinstall for minimist
- Fix new lint issues that came from eslint dependency change
- Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
- Add the adapter type in the package.json
- Add AdapterInfo.js script
- Add json-query dependency
- Add the propertiesDecorators.json for product encryption
-Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
- Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
- Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
- Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
- Add AdapterInfo.json
- Add systemName for documentation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!15

---

## 0.6.0 [08-02-2021]

- Add call for postVcentervmtemplatedeploy

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!14

---

## 0.5.5 [03-16-2021]

- migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!12

---

## 0.5.4 [07-09-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!11

---

## 0.5.3 [06-25-2020]

- Changed the filters so that they are being sent on the request properly
Tested

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!10

---

## 0.5.2 [06-23-2020] & 0.5.1 [01-16-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!9

---

## 0.5.0 [11-08-2019] & 0.4.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!8

---

## 0.3.0 [09-14-2019]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!7

---
## 0.2.8 [07-30-2019] & 0.2.7 [07-16-2019] & 0.2.6 [07-15-2019] & 0.2.5 [07-15-2019]

- Bug fixes and performance improvements. Patch/another artifact

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!6

---

## 0.2.4 [07-15-2019] & 0.2.3 [07-15-2019]

- touch ups for artifact

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!5

---

## 0.2.2 [07-15-2019]

- Test in running pipeline and having things be correct

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!4

---

## 0.2.1 [07-11-2019]

- Merge pipeline changes into master for App Artifact

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!3

---

## 0.2.0 [07-11-2019]

- Migrate the adapter to latest, categroize, and add artifact support

See merge request itentialopensource/adapters/cloud/adapter-vmware_vcenter!2

---

## 0.1.2 [04-23-2019]
- Name can not have uppercase letters

See merge request itentialopensource/adapters/adapter-vmware_vcenter!1

---

## 0.1.1 [04-23-2019]

- Initial Commit

See commit e7c510a

---
