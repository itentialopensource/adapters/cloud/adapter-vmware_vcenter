# VMware vCenter

Vendor: VMware
Homepage: https://www.vmware.com/

Product: VMware vCenter
Product Page: https://www.vmware.com/products/cloud-infrastructure/vcenter

## Introduction
We classify VMware vCenter into the Cloud domain as VMware vCenter controls/orchestrates actions within the Cloud.

"VMware vCenter allows you to automate and deliver a virtual infrastructure across the hybrid cloud."

## Why Integrate
The VMware vCenter adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware vCenter. With this adapter you have the ability to perform operations such as:

- Manage Server Appliance infrastructure in a vSphere environment.

## Additional Product Documentation
The [API documents for VMware vCenter](https://developer.vmware.com/apis/vsphere-automation/latest/vcenter/)