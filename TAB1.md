# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Vmware_vCenter System. The API that was used to build the adapter for Vmware_vCenter is usually available in the report directory of this adapter. The adapter utilizes the Vmware_vCenter API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The VMware vCenter adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VMware vCenter. With this adapter you have the ability to perform operations such as:

- Manage Server Appliance infrastructure in a vSphere environment.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
