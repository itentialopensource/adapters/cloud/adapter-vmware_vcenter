/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-vmware_vcenter',
      type: 'VmwareVCenter',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const VmwareVCenter = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Vmware_vCenter Adapter Test', () => {
  describe('VmwareVCenter Class Tests', () => {
    const a = new VmwareVCenter(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('vmware_vcenter'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.7.3', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.14.2', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('vmware_vcenter'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('VmwareVCenter', pronghornDotJson.export);
          assert.equal('Vmware_vCenter', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-vmware_vcenter', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('vmware_vcenter'));
          assert.equal('VmwareVCenter', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-vmware_vcenter', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-vmware_vcenter', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#postComvmwarevcenterinventorydatastoreactionfind - errors', () => {
      it('should have a postComvmwarevcenterinventorydatastoreactionfind function', (done) => {
        try {
          assert.equal(true, typeof a.postComvmwarevcenterinventorydatastoreactionfind === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postComvmwarevcenterinventorynetworkactionfind - errors', () => {
      it('should have a postComvmwarevcenterinventorynetworkactionfind function', (done) => {
        try {
          assert.equal(true, typeof a.postComvmwarevcenterinventorynetworkactionfind === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postComvmwarevcenterisoimageidlibraryItemactionmount - errors', () => {
      it('should have a postComvmwarevcenterisoimageidlibraryItemactionmount function', (done) => {
        try {
          assert.equal(true, typeof a.postComvmwarevcenterisoimageidlibraryItemactionmount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing libraryItem', (done) => {
        try {
          a.postComvmwarevcenterisoimageidlibraryItemactionmount(null, (data, error) => {
            try {
              const displayE = 'libraryItem is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postComvmwarevcenterisoimageidlibraryItemactionmount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postComvmwarevcenterisoimageidvmactionunmount - errors', () => {
      it('should have a postComvmwarevcenterisoimageidvmactionunmount function', (done) => {
        try {
          assert.equal(true, typeof a.postComvmwarevcenterisoimageidvmactionunmount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postComvmwarevcenterisoimageidvmactionunmount(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postComvmwarevcenterisoimageidvmactionunmount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComvmwarevcenterovfexportFlag - errors', () => {
      it('should have a getComvmwarevcenterovfexportFlag function', (done) => {
        try {
          assert.equal(true, typeof a.getComvmwarevcenterovfexportFlag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComvmwarevcenterovfimportFlag - errors', () => {
      it('should have a getComvmwarevcenterovfimportFlag function', (done) => {
        try {
          assert.equal(true, typeof a.getComvmwarevcenterovfimportFlag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postComvmwarevcenterovflibraryItem - errors', () => {
      it('should have a postComvmwarevcenterovflibraryItem function', (done) => {
        try {
          assert.equal(true, typeof a.postComvmwarevcenterovflibraryItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientTokensourcetargetcreateSpec', (done) => {
        try {
          a.postComvmwarevcenterovflibraryItem(null, (data, error) => {
            try {
              const displayE = 'clientTokensourcetargetcreateSpec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postComvmwarevcenterovflibraryItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postComvmwarevcenterovflibraryItemidovfLibraryItemIdactiondeploy - errors', () => {
      it('should have a postComvmwarevcenterovflibraryItemidovfLibraryItemIdactiondeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postComvmwarevcenterovflibraryItemidovfLibraryItemIdactiondeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ovfLibraryItemId', (done) => {
        try {
          a.postComvmwarevcenterovflibraryItemidovfLibraryItemIdactiondeploy(null, null, (data, error) => {
            try {
              const displayE = 'ovfLibraryItemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postComvmwarevcenterovflibraryItemidovfLibraryItemIdactiondeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientTokentargetdeploymentSpec', (done) => {
        try {
          a.postComvmwarevcenterovflibraryItemidovfLibraryItemIdactiondeploy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientTokentargetdeploymentSpec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postComvmwarevcenterovflibraryItemidovfLibraryItemIdactiondeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postComvmwarevcenterovflibraryItemidovfLibraryItemIdactionfilter - errors', () => {
      it('should have a postComvmwarevcenterovflibraryItemidovfLibraryItemIdactionfilter function', (done) => {
        try {
          assert.equal(true, typeof a.postComvmwarevcenterovflibraryItemidovfLibraryItemIdactionfilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ovfLibraryItemId', (done) => {
        try {
          a.postComvmwarevcenterovflibraryItemidovfLibraryItemIdactionfilter(null, null, (data, error) => {
            try {
              const displayE = 'ovfLibraryItemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postComvmwarevcenterovflibraryItemidovfLibraryItemIdactionfilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing target', (done) => {
        try {
          a.postComvmwarevcenterovflibraryItemidovfLibraryItemIdactionfilter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'target is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postComvmwarevcenterovflibraryItemidovfLibraryItemIdactionfilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentercluster - errors', () => {
      it('should have a getVcentercluster function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentercluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterclustercluster - errors', () => {
      it('should have a getVcenterclustercluster function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterclustercluster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cluster', (done) => {
        try {
          a.getVcenterclustercluster(null, (data, error) => {
            try {
              const displayE = 'cluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcenterclustercluster', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcenterdatacenter - errors', () => {
      it('should have a postVcenterdatacenter function', (done) => {
        try {
          assert.equal(true, typeof a.postVcenterdatacenter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcenterdatacenter(null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcenterdatacenter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterdatacenter - errors', () => {
      it('should have a getVcenterdatacenter function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterdatacenter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterdatacenterdatacenter - errors', () => {
      it('should have a getVcenterdatacenterdatacenter function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterdatacenterdatacenter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing datacenter', (done) => {
        try {
          a.getVcenterdatacenterdatacenter(null, (data, error) => {
            try {
              const displayE = 'datacenter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcenterdatacenterdatacenter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcenterdatacenterdatacenter - errors', () => {
      it('should have a deleteVcenterdatacenterdatacenter function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcenterdatacenterdatacenter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing datacenter', (done) => {
        try {
          a.deleteVcenterdatacenterdatacenter(null, null, (data, error) => {
            try {
              const displayE = 'datacenter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcenterdatacenterdatacenter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterdatastore - errors', () => {
      it('should have a getVcenterdatastore function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterdatastore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterdatastoredatastore - errors', () => {
      it('should have a getVcenterdatastoredatastore function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterdatastoredatastore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing datastore', (done) => {
        try {
          a.getVcenterdatastoredatastore(null, (data, error) => {
            try {
              const displayE = 'datastore is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcenterdatastoredatastore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterfolder - errors', () => {
      it('should have a getVcenterfolder function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterfolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcenterhost - errors', () => {
      it('should have a postVcenterhost function', (done) => {
        try {
          assert.equal(true, typeof a.postVcenterhost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcenterhost(null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcenterhost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterhost - errors', () => {
      it('should have a getVcenterhost function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterhost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcenterhosthost - errors', () => {
      it('should have a deleteVcenterhosthost function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcenterhosthost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing host', (done) => {
        try {
          a.deleteVcenterhosthost(null, (data, error) => {
            try {
              const displayE = 'host is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcenterhosthost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcenterhosthostconnect - errors', () => {
      it('should have a postVcenterhosthostconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcenterhosthostconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing host', (done) => {
        try {
          a.postVcenterhosthostconnect(null, (data, error) => {
            try {
              const displayE = 'host is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcenterhosthostconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcenterhosthostdisconnect - errors', () => {
      it('should have a postVcenterhosthostdisconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcenterhosthostdisconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing host', (done) => {
        try {
          a.postVcenterhosthostdisconnect(null, (data, error) => {
            try {
              const displayE = 'host is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcenterhosthostdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenternetwork - errors', () => {
      it('should have a getVcenternetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenternetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterresourcePool - errors', () => {
      it('should have a getVcenterresourcePool function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterresourcePool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcenterresourcePoolresourcePool - errors', () => {
      it('should have a getVcenterresourcePoolresourcePool function', (done) => {
        try {
          assert.equal(true, typeof a.getVcenterresourcePoolresourcePool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourcepool', (done) => {
        try {
          a.getVcenterresourcePoolresourcePool(null, (data, error) => {
            try {
              const displayE = 'resourcepool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcenterresourcePoolresourcePool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervm - errors', () => {
      it('should have a postVcentervm function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervm(null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervm - errors', () => {
      it('should have a getVcentervm function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvm - errors', () => {
      it('should have a deleteVcentervmvm function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvm(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvm - errors', () => {
      it('should have a getVcentervmvm function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvm(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardware - errors', () => {
      it('should have a getVcentervmvmhardware function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardware === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardware(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardware', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardware - errors', () => {
      it('should have a patchVcentervmvmhardware function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardware === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardware(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardware', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardware('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardware', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareactionupgrade - errors', () => {
      it('should have a postVcentervmvmhardwareactionupgrade function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareactionupgrade === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareactionupgrade(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareactionupgrade', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareadaptersata - errors', () => {
      it('should have a postVcentervmvmhardwareadaptersata function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareadaptersata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareadaptersata(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareadaptersata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervmvmhardwareadaptersata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareadaptersata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareadaptersata - errors', () => {
      it('should have a getVcentervmvmhardwareadaptersata function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareadaptersata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareadaptersata(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareadaptersata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvmhardwareadaptersataadapter - errors', () => {
      it('should have a deleteVcentervmvmhardwareadaptersataadapter function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvmhardwareadaptersataadapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvmhardwareadaptersataadapter(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareadaptersataadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adapter', (done) => {
        try {
          a.deleteVcentervmvmhardwareadaptersataadapter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'adapter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareadaptersataadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareadaptersataadapter - errors', () => {
      it('should have a getVcentervmvmhardwareadaptersataadapter function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareadaptersataadapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareadaptersataadapter(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareadaptersataadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adapter', (done) => {
        try {
          a.getVcentervmvmhardwareadaptersataadapter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'adapter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareadaptersataadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareadapterscsi - errors', () => {
      it('should have a postVcentervmvmhardwareadapterscsi function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareadapterscsi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareadapterscsi(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareadapterscsi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervmvmhardwareadapterscsi('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareadapterscsi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareadapterscsi - errors', () => {
      it('should have a getVcentervmvmhardwareadapterscsi function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareadapterscsi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareadapterscsi(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareadapterscsi', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareadapterscsiadapter - errors', () => {
      it('should have a getVcentervmvmhardwareadapterscsiadapter function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareadapterscsiadapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareadapterscsiadapter(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareadapterscsiadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adapter', (done) => {
        try {
          a.getVcentervmvmhardwareadapterscsiadapter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'adapter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareadapterscsiadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwareadapterscsiadapter - errors', () => {
      it('should have a patchVcentervmvmhardwareadapterscsiadapter function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwareadapterscsiadapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwareadapterscsiadapter(null, null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareadapterscsiadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adapter', (done) => {
        try {
          a.patchVcentervmvmhardwareadapterscsiadapter('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'adapter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareadapterscsiadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwareadapterscsiadapter('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareadapterscsiadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvmhardwareadapterscsiadapter - errors', () => {
      it('should have a deleteVcentervmvmhardwareadapterscsiadapter function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvmhardwareadapterscsiadapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvmhardwareadapterscsiadapter(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareadapterscsiadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adapter', (done) => {
        try {
          a.deleteVcentervmvmhardwareadapterscsiadapter('fakeparam', null, (data, error) => {
            try {
              const displayE = 'adapter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareadapterscsiadapter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareboot - errors', () => {
      it('should have a getVcentervmvmhardwareboot function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareboot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareboot(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareboot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwareboot - errors', () => {
      it('should have a patchVcentervmvmhardwareboot function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwareboot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwareboot(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareboot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwareboot('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareboot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVcentervmvmhardwarebootdevice - errors', () => {
      it('should have a putVcentervmvmhardwarebootdevice function', (done) => {
        try {
          assert.equal(true, typeof a.putVcentervmvmhardwarebootdevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.putVcentervmvmhardwarebootdevice(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-putVcentervmvmhardwarebootdevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devices', (done) => {
        try {
          a.putVcentervmvmhardwarebootdevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'devices is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-putVcentervmvmhardwarebootdevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwarebootdevice - errors', () => {
      it('should have a getVcentervmvmhardwarebootdevice function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwarebootdevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwarebootdevice(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarebootdevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwarecdrom - errors', () => {
      it('should have a postVcentervmvmhardwarecdrom function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwarecdrom === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwarecdrom(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarecdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervmvmhardwarecdrom('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarecdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwarecdrom - errors', () => {
      it('should have a getVcentervmvmhardwarecdrom function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwarecdrom === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwarecdrom(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarecdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwarecdromcdrom - errors', () => {
      it('should have a patchVcentervmvmhardwarecdromcdrom function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwarecdromcdrom === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwarecdromcdrom(null, null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarecdromcdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cdrom', (done) => {
        try {
          a.patchVcentervmvmhardwarecdromcdrom('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'cdrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarecdromcdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwarecdromcdrom('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarecdromcdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwarecdromcdrom - errors', () => {
      it('should have a getVcentervmvmhardwarecdromcdrom function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwarecdromcdrom === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwarecdromcdrom(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarecdromcdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cdrom', (done) => {
        try {
          a.getVcentervmvmhardwarecdromcdrom('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cdrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarecdromcdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvmhardwarecdromcdrom - errors', () => {
      it('should have a deleteVcentervmvmhardwarecdromcdrom function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvmhardwarecdromcdrom === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvmhardwarecdromcdrom(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwarecdromcdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cdrom', (done) => {
        try {
          a.deleteVcentervmvmhardwarecdromcdrom('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cdrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwarecdromcdrom', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwarecdromcdromconnect - errors', () => {
      it('should have a postVcentervmvmhardwarecdromcdromconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwarecdromcdromconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwarecdromcdromconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarecdromcdromconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cdrom', (done) => {
        try {
          a.postVcentervmvmhardwarecdromcdromconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cdrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarecdromcdromconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwarecdromcdromdisconnect - errors', () => {
      it('should have a postVcentervmvmhardwarecdromcdromdisconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwarecdromcdromdisconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwarecdromcdromdisconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarecdromcdromdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cdrom', (done) => {
        try {
          a.postVcentervmvmhardwarecdromcdromdisconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cdrom is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarecdromcdromdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwarecpu - errors', () => {
      it('should have a getVcentervmvmhardwarecpu function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwarecpu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwarecpu(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarecpu', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwarecpu - errors', () => {
      it('should have a patchVcentervmvmhardwarecpu function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwarecpu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwarecpu(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarecpu', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwarecpu('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarecpu', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwaredisk - errors', () => {
      it('should have a postVcentervmvmhardwaredisk function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwaredisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwaredisk(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwaredisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervmvmhardwaredisk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwaredisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwaredisk - errors', () => {
      it('should have a getVcentervmvmhardwaredisk function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwaredisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwaredisk(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwaredisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwarediskdisk - errors', () => {
      it('should have a getVcentervmvmhardwarediskdisk function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwarediskdisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwarediskdisk(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarediskdisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing disk', (done) => {
        try {
          a.getVcentervmvmhardwarediskdisk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'disk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarediskdisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvmhardwarediskdisk - errors', () => {
      it('should have a deleteVcentervmvmhardwarediskdisk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvmhardwarediskdisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvmhardwarediskdisk(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwarediskdisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing disk', (done) => {
        try {
          a.deleteVcentervmvmhardwarediskdisk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'disk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwarediskdisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwarediskdisk - errors', () => {
      it('should have a patchVcentervmvmhardwarediskdisk function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwarediskdisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwarediskdisk(null, null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarediskdisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing disk', (done) => {
        try {
          a.patchVcentervmvmhardwarediskdisk('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'disk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarediskdisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwarediskdisk('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarediskdisk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareethernet - errors', () => {
      it('should have a postVcentervmvmhardwareethernet function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareethernet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareethernet(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareethernet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervmvmhardwareethernet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareethernet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareethernet - errors', () => {
      it('should have a getVcentervmvmhardwareethernet function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareethernet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareethernet(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareethernet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwareethernetnic - errors', () => {
      it('should have a patchVcentervmvmhardwareethernetnic function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwareethernetnic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwareethernetnic(null, null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareethernetnic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nic', (done) => {
        try {
          a.patchVcentervmvmhardwareethernetnic('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nic is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareethernetnic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwareethernetnic('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareethernetnic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareethernetnic - errors', () => {
      it('should have a getVcentervmvmhardwareethernetnic function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareethernetnic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareethernetnic(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareethernetnic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nic', (done) => {
        try {
          a.getVcentervmvmhardwareethernetnic('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nic is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareethernetnic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvmhardwareethernetnic - errors', () => {
      it('should have a deleteVcentervmvmhardwareethernetnic function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvmhardwareethernetnic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvmhardwareethernetnic(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareethernetnic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nic', (done) => {
        try {
          a.deleteVcentervmvmhardwareethernetnic('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nic is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareethernetnic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareethernetnicconnect - errors', () => {
      it('should have a postVcentervmvmhardwareethernetnicconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareethernetnicconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareethernetnicconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareethernetnicconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nic', (done) => {
        try {
          a.postVcentervmvmhardwareethernetnicconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nic is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareethernetnicconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareethernetnicdisconnect - errors', () => {
      it('should have a postVcentervmvmhardwareethernetnicdisconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareethernetnicdisconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareethernetnicdisconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareethernetnicdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nic', (done) => {
        try {
          a.postVcentervmvmhardwareethernetnicdisconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nic is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareethernetnicdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwarefloppy - errors', () => {
      it('should have a postVcentervmvmhardwarefloppy function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwarefloppy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwarefloppy(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarefloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervmvmhardwarefloppy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarefloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwarefloppy - errors', () => {
      it('should have a getVcentervmvmhardwarefloppy function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwarefloppy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwarefloppy(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarefloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvmhardwarefloppyfloppy - errors', () => {
      it('should have a deleteVcentervmvmhardwarefloppyfloppy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvmhardwarefloppyfloppy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvmhardwarefloppyfloppy(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwarefloppyfloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floppy', (done) => {
        try {
          a.deleteVcentervmvmhardwarefloppyfloppy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'floppy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwarefloppyfloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwarefloppyfloppy - errors', () => {
      it('should have a patchVcentervmvmhardwarefloppyfloppy function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwarefloppyfloppy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwarefloppyfloppy(null, null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarefloppyfloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floppy', (done) => {
        try {
          a.patchVcentervmvmhardwarefloppyfloppy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'floppy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarefloppyfloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwarefloppyfloppy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarefloppyfloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwarefloppyfloppy - errors', () => {
      it('should have a getVcentervmvmhardwarefloppyfloppy function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwarefloppyfloppy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwarefloppyfloppy(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarefloppyfloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floppy', (done) => {
        try {
          a.getVcentervmvmhardwarefloppyfloppy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'floppy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarefloppyfloppy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwarefloppyfloppyconnect - errors', () => {
      it('should have a postVcentervmvmhardwarefloppyfloppyconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwarefloppyfloppyconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwarefloppyfloppyconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarefloppyfloppyconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floppy', (done) => {
        try {
          a.postVcentervmvmhardwarefloppyfloppyconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'floppy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarefloppyfloppyconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwarefloppyfloppydisconnect - errors', () => {
      it('should have a postVcentervmvmhardwarefloppyfloppydisconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwarefloppyfloppydisconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwarefloppyfloppydisconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarefloppyfloppydisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floppy', (done) => {
        try {
          a.postVcentervmvmhardwarefloppyfloppydisconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'floppy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwarefloppyfloppydisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwarememory - errors', () => {
      it('should have a patchVcentervmvmhardwarememory function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwarememory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwarememory(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarememory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwarememory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwarememory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwarememory - errors', () => {
      it('should have a getVcentervmvmhardwarememory function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwarememory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwarememory(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwarememory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareparallel - errors', () => {
      it('should have a postVcentervmvmhardwareparallel function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareparallel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareparallel(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareparallel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervmvmhardwareparallel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareparallel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareparallel - errors', () => {
      it('should have a getVcentervmvmhardwareparallel function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareparallel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareparallel(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareparallel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvmhardwareparallelport - errors', () => {
      it('should have a deleteVcentervmvmhardwareparallelport function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvmhardwareparallelport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvmhardwareparallelport(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareparallelport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.deleteVcentervmvmhardwareparallelport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareparallelport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwareparallelport - errors', () => {
      it('should have a patchVcentervmvmhardwareparallelport function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwareparallelport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwareparallelport(null, null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareparallelport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.patchVcentervmvmhardwareparallelport('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareparallelport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwareparallelport('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareparallelport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareparallelport - errors', () => {
      it('should have a getVcentervmvmhardwareparallelport function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareparallelport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareparallelport(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareparallelport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.getVcentervmvmhardwareparallelport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareparallelport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareparallelportconnect - errors', () => {
      it('should have a postVcentervmvmhardwareparallelportconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareparallelportconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareparallelportconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareparallelportconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.postVcentervmvmhardwareparallelportconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareparallelportconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareparallelportdisconnect - errors', () => {
      it('should have a postVcentervmvmhardwareparallelportdisconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareparallelportdisconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareparallelportdisconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareparallelportdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.postVcentervmvmhardwareparallelportdisconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareparallelportdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareserial - errors', () => {
      it('should have a postVcentervmvmhardwareserial function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareserial === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareserial(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareserial', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.postVcentervmvmhardwareserial('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareserial', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareserial - errors', () => {
      it('should have a getVcentervmvmhardwareserial function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareserial === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareserial(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareserial', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVcentervmvmhardwareserialport - errors', () => {
      it('should have a patchVcentervmvmhardwareserialport function', (done) => {
        try {
          assert.equal(true, typeof a.patchVcentervmvmhardwareserialport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.patchVcentervmvmhardwareserialport(null, null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareserialport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.patchVcentervmvmhardwareserialport('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareserialport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.patchVcentervmvmhardwareserialport('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-patchVcentervmvmhardwareserialport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmhardwareserialport - errors', () => {
      it('should have a getVcentervmvmhardwareserialport function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmhardwareserialport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmhardwareserialport(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareserialport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.getVcentervmvmhardwareserialport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmhardwareserialport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVcentervmvmhardwareserialport - errors', () => {
      it('should have a deleteVcentervmvmhardwareserialport function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVcentervmvmhardwareserialport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.deleteVcentervmvmhardwareserialport(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareserialport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.deleteVcentervmvmhardwareserialport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-deleteVcentervmvmhardwareserialport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareserialportconnect - errors', () => {
      it('should have a postVcentervmvmhardwareserialportconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareserialportconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareserialportconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareserialportconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.postVcentervmvmhardwareserialportconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareserialportconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmhardwareserialportdisconnect - errors', () => {
      it('should have a postVcentervmvmhardwareserialportdisconnect function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmhardwareserialportdisconnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmhardwareserialportdisconnect(null, null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareserialportdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.postVcentervmvmhardwareserialportdisconnect('fakeparam', null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmhardwareserialportdisconnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVcentervmvmpower - errors', () => {
      it('should have a getVcentervmvmpower function', (done) => {
        try {
          assert.equal(true, typeof a.getVcentervmvmpower === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.getVcentervmvmpower(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-getVcentervmvmpower', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmpowerreset - errors', () => {
      it('should have a postVcentervmvmpowerreset function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmpowerreset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmpowerreset(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmpowerreset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmpowerstart - errors', () => {
      it('should have a postVcentervmvmpowerstart function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmpowerstart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmpowerstart(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmpowerstart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmpowerstop - errors', () => {
      it('should have a postVcentervmvmpowerstop function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmpowerstop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmpowerstop(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmpowerstop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmvmpowersuspend - errors', () => {
      it('should have a postVcentervmvmpowersuspend function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmvmpowersuspend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vm', (done) => {
        try {
          a.postVcentervmvmpowersuspend(null, (data, error) => {
            try {
              const displayE = 'vm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmvmpowersuspend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcentervmtemplatedeploy - errors', () => {
      it('should have a postVcentervmtemplatedeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postVcentervmtemplatedeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing item', (done) => {
        try {
          a.postVcentervmtemplatedeploy(null, null, (data, error) => {
            try {
              const displayE = 'item is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmtemplatedeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.postVcentervmtemplatedeploy('fakedata', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-postVcentervmtemplatedeploy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findLibrary - errors', () => {
      it('should have a findLibrary function', (done) => {
        try {
          assert.equal(true, typeof a.findLibrary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.findLibrary(null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-findLibrary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.findLibrary('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-findLibrary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findLibraryItem - errors', () => {
      it('should have a findLibraryItem function', (done) => {
        try {
          assert.equal(true, typeof a.findLibraryItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.findLibraryItem(null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-findLibraryItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.findLibraryItem('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-vmware_vcenter-adapter-findLibraryItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
